package com.example.week3example;

import android.util.Log;

import java.util.Random;

public class Guess {

    public int valueToGuess;
    public int GuessValue;


    public Guess(){
    };


    public void startGuess(){

        Log.d(String.valueOf(MakeGuess(GuessValue)),"Is the guess correct?");
        Log.d(String.valueOf(GuessValue),"the user input within the guess class is it correct?");

    };

    public void setNewValue(){ //set a random value between 1 and 10 as the value to be guessed.
        Random ran = new Random();
        valueToGuess = ran.nextInt(11);
     //   Log.d(String.valueOf(valueToGuess),"Random value");
    };

    public boolean MakeGuess(int GuessValue) { // if value guessed correctly return true if not return false.
        if(valueToGuess == GuessValue){
            return true;
        }else {
            return false;
        }
    };
}
