package com.example.week3example;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    EditText inputnumber;

    public int guesses_left = 3;
    public int user_guess;
    public int score = 0;
    Guess GuessObject = new Guess();

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        // Save the user's current game state
        savedInstanceState.putInt("guesses left", guesses_left);

        // Always call the superclass so it can save the view hierarchy state
        super.onSaveInstanceState(savedInstanceState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        guesses_left = savedInstanceState.getInt("guesses left");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        GuessObject.setNewValue(); // set the random value to guess

        inputnumber = (EditText) findViewById(R.id.inputnumber); // set the edittext to input a number to guess


        final TextView Guesses = (TextView) findViewById(R.id.guesstext); // field for guesses left text
        final TextView guessleft = (TextView) findViewById(R.id.guessleft); // field for guesses left digit
        final TextView Instructions = (TextView) findViewById(R.id.instructiontext); // field for instruction text
        Instructions.setText(R.string.instructions); // correct guess message


        final Button Submit = (Button) findViewById(R.id.submitbtn);
        Submit.setText(R.string.guess_button);// 'guess_button' is name in sting file , to change button text to 'Guess'
        Submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Guesses.setText(R.string.guesses_left_message); // set Guesses left text from string
                guessleft.setText(String.valueOf(guesses_left)); // set number of guesses left

                user_guess = Integer.valueOf(inputnumber.getText().toString()); // retrieve the users input
                GuessObject.MakeGuess(user_guess); // check whether the user has guessed correctly or not

                Log.d(String.valueOf(GuessObject.MakeGuess(user_guess)), "if match true if no match then false"); // TEST
                TextView Submit = (TextView) findViewById(R.id.submitbtn);
                TextView Instructions = (TextView) findViewById(R.id.instructiontext); // field for instruction text

                if (guesses_left > 0) {

                    if (GuessObject.MakeGuess(user_guess) == true)   // if guessed correctly
                    {
                        Submit.setText(R.string.play_again_button);// 'play_again_button' is name in sting file , to change button text to 'play_again'
                        Instructions.setText(R.string.instructions); // correct guess message
                        GuessObject.setNewValue(); // set new random value to guess
                        guesses_left = 3; //RESET CHANCES BACK TO 3
                        guessleft.setText(String.valueOf(guesses_left)); // set number of guesses left
                        score++;//increase the score value by 1 for every correct guess
                    } else // if not guessed correctly
                    {
                        Submit.setText(R.string.try_again_button);// 'try_again_button' is name in sting file , to change button text to 'Try again'
                        Instructions.setText(R.string.incorrect_guess); // incorrect guess message
                        guesses_left--;
                    }
                } else {
                    Instructions.setText(R.string.lost); // lost message
                    // SAVE THE SCORE TO THE LEADERBOARD

                }

                Log.d(String.valueOf(user_guess), "User number input"); //TEST
                Log.d(String.valueOf(GuessObject.valueToGuess), "random value"); // TEST
                Log.d(String.valueOf(guesses_left), "number of guesses left"); // TEST
                Log.d(String.valueOf(score), "The highscore"); // TEST


            }
        });
      /*  //save the score to leaderboard
        public void saveData (View view){
            SharedPreferences sharedPref = getSharedPreferences(PREFS_NAME, 0);
            SharedPreferences.Editor editor = sharedPref.edit();
            editor.putInt(PREFS_NAME, score);
            editor.apply();

            Toast.makeText(this, "Saved to highscore", Toast.LENGTH_SHORT).show();
        }

        //Print out the highscore
        public void loadData (View view){
            SharedPreferences sharedPref = getSharedPreferences(PREFS_NAME, 0);
            String score1 = sharedPref.getString(PREFS_NAME, "");

            Instructions.setText(PREFS_NAME);
        }
        */
    }
}

